commands
----------

#### upload spifss

```bash
pio run --target uploadfs
```

#### git secret

to pull a secret

```bash
git secret changes
git secret hide
```

to fetch a secret

```bash
git secret reveal
```
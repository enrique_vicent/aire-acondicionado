#if !defined(_CONSTANTS_HPP_)
#define _CONSTANTS_HPP_

#include <Arduino.h>

//pinout
#define PIN_LED 21
#define PIN_OTA_SWITCH 2
#define PIN_SERVO_DATA 23
#define PIN_SERVO_ENABLE 25
#define PIN_BUTTON 15
#define PIN_VVIN A5 //33 
#define PIN_LIGHT_DATA A7 //35  not working with wifi on
#define PIN_LIGHT_EN 12

//pusher constants
#define CLICK_POSITION 138
#define RELEASE_POSITION 90 
#define CLICK_TIME_MILES 900
#define ENABLE_TIME_MILES 25 

//ota constants
#define OTA_RETRIES 50 // > 5s
#define HOTA_HOSTNAME "aircondplug"

//espnow constans
#define ESPNOW_HOSTNAME "aircond"
#define ESPNOW_TIMEOUT 1000

//caliration constants
#define VIN_READ_MAX 2180 //4.2v
#define VIN_READ_MIN 2022 //3.6v a menos no se mueve el servo
#define VIN_READ_SAMPLES 32
#define VIN_THRESOLD 100

//sleep
#define DEFAULT_SLEEP_TIME_MILLIS 70L * 1000L
#define MAX_EXEC_TIME_MILLIS 10*1000


#endif // _CONSTANTS_HPP_
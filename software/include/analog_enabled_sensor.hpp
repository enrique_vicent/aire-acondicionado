#ifndef _analog_enabled_sensor_hpp_
#define _analog_enabled_sensor_hpp_

#include <Arduino.h>
#include "enabler.hpp"
#include "analog_sensor.hpp"

class AnalogEnabledSensor {
  private:
    AnalogSensor sensor;
    Enabler en;
  public:
    AnalogEnabledSensor (int dataPin, int enablePin, long wakeUpMilis = 0l , long wakeDownMilis=0l, bool enableInverse =false ):
      sensor(dataPin),
      en(enablePin, wakeUpMilis, wakeDownMilis, enableInverse)
    {}
    void setup(){
      en.setup();
    }
    int read(int times=1 ){
      en.enable();
      int result = sensor.read(times);
      en.disable();
      return result;
    }
};


#endif
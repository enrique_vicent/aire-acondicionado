#ifndef _button_hpp_
#define _button_hpp_

#include <Arduino.h>

//basic polling button
class PolledButton{
  private:
    int pin;
    int mode;
    bool inverted;
  public:
    PolledButton(int pin, bool inverted = true ,int mode=INPUT_PULLUP ){
      this->pin = pin;
      this->mode = mode;
      this->inverted = inverted;

      pinMode(pin, mode);
    }

    bool isPressed(){
      int read = digitalRead(pin);
      bool result = (read==HIGH) != (this->inverted);
      return result;
    }
};

#endif
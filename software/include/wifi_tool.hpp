#ifndef _wifi_tool_hpp_
#define _wifi_tool_hpp_

#include <Arduino.h>
#include <WiFi.h>

void setupWiFi(const char* ssid, const char* password){
    WiFi.persistent( false );
    WiFi.begin(ssid, password); 
    Serial.print("wifiConnect ");
    while (WiFi.status() != WL_CONNECTED) { // Wait for the Wi-Fi to connect
        delay(100);
        Serial.print('.');
        //TODO: cap retries
    }
    Serial.println("Connection established!");
    Serial.print("IP address:\t");
    Serial.println(WiFi.localIP());  
}

void endWifi(){
    WiFi.disconnect();
    WiFi.mode(WIFI_OFF);
}

#endif
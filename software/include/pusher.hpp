#if !defined(_pusher_hpp_)
#define _pusher_hpp_

#include <Servo.h>
#include "enabler.hpp"

class Pusher
{
private:
    Servo servo;
    Enabler en;
public:
    Pusher(int servoPin, int enablePin) 
        :en (enablePin, ENABLE_TIME_MILES) {
        servo.attach(servoPin); 
    }
    ~Pusher() {}
    void setup(){en.setup();}
    void push(int clickPosition, int releasePosition, long timeMillis);
private:
    void doPush(int clickPosition, int releasePosition, long timeMillis);
};

void Pusher::push(int clickPosition, int releasePosition, long timeMillis){
    en.enable();
    doPush(clickPosition, releasePosition, timeMillis);
    en.disable();
}
void Pusher::doPush(int clickPosition, int releasePosition, long timeMillis){
    servo.write(clickPosition);
    delay(timeMillis);
    servo.write(releasePosition);
    delay(timeMillis);
}

#endif // _pusher_hpp_

#ifndef _sleep_hpp_
#define _sleep_hpp_

#include <Arduino.h>
#include "constants.hpp"

RTC_DATA_ATTR long Sleep_sleepTimeMillis = DEFAULT_SLEEP_TIME_MILLIS;

/**
 * see https://randomnerdtutorials.com/esp32-deep-sleep-arduino-ide-wake-up-sources/
 */
class Sleep {
  gpio_num_t pinGpioExt;
  int level; //1=high, 0=low
  public:
    Sleep(gpio_num_t extPin, int extLevel=0);
    void goToSleep();
    int whoAwakedMe();
};

Sleep::Sleep(gpio_num_t extPin, int extLevel){
  this->pinGpioExt = extPin;
  this->level = extLevel;
}

void Sleep::goToSleep(){
  long timeMicros = Sleep_sleepTimeMillis * 1000L;
  esp_sleep_enable_ext0_wakeup(pinGpioExt, level);
  esp_sleep_enable_timer_wakeup(timeMicros);
  esp_deep_sleep_start();
}

/*
switch(whoAwakedMe){
  case ESP_SLEEP_WAKEUP_EXT0 : Serial.println("Wakeup caused by external signal using RTC_IO"); break;
  case ESP_SLEEP_WAKEUP_EXT1 : Serial.println("Wakeup caused by external signal using RTC_CNTL"); break;
  case ESP_SLEEP_WAKEUP_TIMER : Serial.println("Wakeup caused by timer"); break;
  case ESP_SLEEP_WAKEUP_TOUCHPAD : Serial.println("Wakeup caused by touchpad"); break;
  case ESP_SLEEP_WAKEUP_ULP : Serial.println("Wakeup caused by ULP program"); break;
  default : Serial.printf("Wakeup was not caused by deep sleep: %d\n",wakeup_reason); break;
}*/
int Sleep::whoAwakedMe(){
  esp_sleep_wakeup_cause_t wakeup_reason;
  wakeup_reason = esp_sleep_get_wakeup_cause();
  return wakeup_reason;
}

#endif

#ifndef __TIMEOUT_HPP_
#define __TIMEOUT_HPP_
//kills execution if hangs more than an specified ammount
//usually occurs when wifi cannot connect

#include <Arduino.h>

xTimerHandle __TimeoutWatchDog_timerHandle;
static void __TimeoutWatchDog_callbackWatchdog(xTimerHandle h){
  esp_sleep_enable_timer_wakeup(1000);
  esp_deep_sleep_start();
}

void setupWatchdog(int timems){
    TimerCallbackFunction_t callback = __TimeoutWatchDog_callbackWatchdog;
    __TimeoutWatchDog_timerHandle = xTimerCreate("watchdogTimeout", //name
            pdMS_TO_TICKS( timems), //period
            pdFALSE, //autoreload
            (void*) 0, //timer id
            callback);
    xTimerStart(__TimeoutWatchDog_timerHandle,0);
}

#endif
#ifndef _comms_hpp_
#define _comms_hpp_

#include <Arduino.h>
#include "secrets.h"
#include "formats.hpp"
#include "EspNow2MqttClient.hpp"


const char * statusString(int data){
    return data>VIN_THRESOLD ? "ON" : "OFF";
}

float batteryPercentaje(const int raw){
    float A = VIN_READ_MIN;
    float B = VIN_READ_MAX;
    float C = 0;
    float D = 100;
    float voltage = (raw -A ) / (B-A) * (D-C) + C;
    return voltage;
}

String getJson(int lsensor, const int battery, long tsleep){
    float batteryPercent = batteryPercentaje(battery);
    
    String result  
      = "{\"battery\":"+        toStringF(batteryPercent, 20) 
      + ",\"battery_raw\":" +   toStringL(battery, 20)
      + ",\"status_raw\":" +    toStringL(lsensor, 20)
      + ",\"sleep_millis\":" +  toStringL(tsleep, 20)
      + ",\"status\":\"" +      statusString(lsensor)
      + "\"}";

    return result;
}

inline void initMessage (EspNow2MqttClient & client, request & requests){
    //most of the message does not change, we create here the scaffold, this is an optimization
    requests.operations[0] = client.createRequestOperationSubscribeQueue("cmd");
    requests.operations[1] = client.createRequestOperationSend("<to fill>","status"); 
    requests.operations[2] = client.createRequestOperationSubscribeQueue("sleep");
    requests.operations_count = 3;  
}

void reSendData (EspNow2MqttClient & client, 
               request & requests, 
               int lsensor, const int battery, long tsleep){
    //most of the message does not change, therefore que focus on the only field actually changes
    String output = getJson(lsensor, battery, tsleep); 
    strncpy(requests.operations[1].op.send.payload, output.c_str(),128);
    client.doRequests(requests); 
}

#endif
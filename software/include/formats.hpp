
#ifndef _formats_hpp_
#define _formats_hpp_

#include <Arduino.h>
#include <string.h>

String toStringF(float data, int bufferSize){
    char Buffer[bufferSize];                
    //4 is mininum width, 2 is precision; float value is copied onto buff
    dtostrf(data, 4, 2, Buffer);
    String result (Buffer);
    return result;
}

String toStringL(long data, int bufferSize){
    char Buffer[bufferSize];                
    ltoa(data, Buffer, 10);
    String result (Buffer);
    return result;
}

#endif
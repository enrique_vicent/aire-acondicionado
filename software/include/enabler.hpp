#ifndef _Enabbler_hpp_
#define _Enabbler_hpp_

#include <Arduino.h>

class Enabler{
  private:
    long wakeUpTimeMilis;
    long wakeDownTimeMilis;
    int enablePin;
    bool inverse;
    int enableLevel;
    int disableLevel;
  public:
    Enabler (int pin, long wakeUpMilis = 0l , long wakeDownMilis=0l, bool inverse =false ){
      enablePin = pin;
      wakeUpTimeMilis = wakeUpMilis;
      wakeDownTimeMilis = wakeDownMilis;
      enableLevel = inverse?LOW:HIGH;
      disableLevel = inverse?HIGH:LOW;
    }
    void setup(){
      pinMode(enablePin, OUTPUT);
    }
    void enable(){
      digitalWrite(enablePin,enableLevel);
      if (wakeUpTimeMilis) delay(wakeUpTimeMilis);
    }
    void disable(){
      digitalWrite(enablePin,disableLevel);
      if (wakeDownTimeMilis) delay (wakeDownTimeMilis);
    }
};


#endif
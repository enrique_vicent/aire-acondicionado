#ifndef _config_hpp_
#define _config_hpp_

#include <FS.h>
#include <Arduino.h>


class AISettings
{
private:
public:
    char * filename = "/settings"; 
    bool ota = false;
    long sleepTimeNanos = 5e6l;
    AISettings(/* args */);
    ~AISettings();
    bool read();
    bool write();
};

AISettings::AISettings(/* args */)
{
    read();
}

AISettings::~AISettings()
{
}

bool AISettings::read()
{
    if (SPIFFS.begin())
    {        
        File file = SPIFFS.open(this->filename, "r");
        if (file)
        {
            ota = file.parseInt()?true:false;
            file.read();
            sleepTimeNanos = file.parseInt();                         
            file.close();
            return true;
        }
        else 
        {            
            return false;
        }
    } 
    else
    {
        return false;
    } 

}

bool AISettings::write()
{
    if (SPIFFS.begin())
    {
        File file = SPIFFS.open(this->filename, "w");
        if (file)
        {
            file.write(ota?'1':'0');
            file.write('\n');
            file.println(sleepTimeNanos);
            file.close();
            return true;
        }
        else 
        {
            return false;
        }
    } 
    else
    {
        return false;
    } 
}

#endif
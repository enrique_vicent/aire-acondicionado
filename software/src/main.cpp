#include <Arduino.h>
#include <string.h>
#include <stdint.h>
#include "constants.hpp"
#include "secrets.hpp"
#include "OTAUtil.hpp"
#include "wifi_tool.hpp"
#include "pusher.hpp"
#include "analog_sensor.hpp"
#include "analog_enabled_sensor.hpp"
#include "button.hpp"
#include "led.hpp"
#include "sleep.hpp"
#include "timeout.hpp"
#include "comms.hpp"
#include <EspNow2MqttClient.hpp>


void commResponseCallback( response & rsp);

RTC_DATA_ATTR int sharedChannel = 0;
EspNow2MqttClient client = EspNow2MqttClient(ESPNOW_HOSTNAME, sharedKey, gatewayMac);
request requests = client.createRequest();
const request_Operation subscribeOperation = client.createRequestOperationSubscribeQueue("cmd");

Pusher pusher(PIN_SERVO_DATA, PIN_SERVO_ENABLE);
PolledButton otaSwitch(PIN_OTA_SWITCH);
PolledButton button(PIN_BUTTON);
Led otaLed(PIN_LED);
WiFiClient wifiClient;
AnalogSensor batterySensor(PIN_VVIN);
AnalogEnabledSensor onOffSensor(PIN_LIGHT_DATA,PIN_LIGHT_EN);
Sleep sleeper ((gpio_num_t)PIN_BUTTON);


/* read OTA siwtch
 * if active then do no operate, we are in programing mode 
 * OTA means Over The Air (programming)
 */
bool otaFlag = true;
// smaphore to know when all task finished
SemaphoreHandle_t sleepSemaphore = xSemaphoreCreateMutex();
//flag that indicates we dont need to wait more for esp-now reply
bool weHaveEspNowResponse = false;


void taskButton (void *pvParameters){
  pusher.setup();
  for(;;){
    if ( button.isPressed() && (pdTRUE == xSemaphoreTake(sleepSemaphore, portMAX_DELAY))){
      pusher.push(CLICK_POSITION, RELEASE_POSITION, CLICK_TIME_MILES);
      xSemaphoreGive(sleepSemaphore);
    }
    delay(10);
  }
}

int32_t getWiFiChannel(const char *ssid) {
  if (int32_t n = WiFi.scanNetworks()) {
      for (uint8_t i=0; i<n; i++) {
          if (!strcmp(ssid, WiFi.SSID(i).c_str())) {
              return WiFi.channel(i);
          }
      }
  }
  return 0;
}

void updateStatus () {
  onOffSensor.setup();
  auto onOffRead = onOffSensor.read(3);
  auto batteryread = batterySensor.read(VIN_READ_SAMPLES);
  reSendData(client, requests, onOffRead, batteryread, Sleep_sleepTimeMillis);
}

//usual setup

void setupForReport() {
  xTaskCreate(taskButton,"boton_servo",10000, NULL, 1, NULL);
  setupWatchdog(MAX_EXEC_TIME_MILLIS);

  client.init(sharedChannel);
  initMessage(client, requests);
  client.onReceiveSomething = commResponseCallback;
  updateStatus();
  
  do {
    delay(1);
  } while (!weHaveEspNowResponse || millis() > ESPNOW_TIMEOUT); //wait for esp response

  while (pdFALSE == xSemaphoreTake(sleepSemaphore, 10)) {// wait for servo to end 
    yield();
  }
};

void setupForClick(){
  pusher.setup();
  pusher.push(CLICK_POSITION, RELEASE_POSITION, CLICK_TIME_MILES);
};

void setupForOTA(){
    setupWiFi(ssid,password);
    InitOTA(HOTA_HOSTNAME);
    do {
      ArduinoOTA.handle();
      yield();
      delay(100);
      otaFlag = otaSwitch.isPressed();
      otaLed.update(otaFlag);
    } while(otaFlag);
    endWifi();
};

void setup() {
  otaFlag = otaSwitch.isPressed();
  otaLed.update(otaFlag);
  if (otaFlag) {
    setupForOTA();
  }
  else { //normal operation awake
    switch(sleeper.whoAwakedMe()){
      case ESP_SLEEP_WAKEUP_EXT0 : 
        setupForClick();
        break;
      //case ESP_SLEEP_WAKEUP_EXT1 : break; 
      case ESP_SLEEP_WAKEUP_TIMER : 
        break;
      //case ESP_SLEEP_WAKEUP_TOUCHPAD : break;
      //case ESP_SLEEP_WAKEUP_ULP : break;
      default:
        sharedChannel = getWiFiChannel(ssid); 
        break;
    }
    setupForReport(); 
  }
  sleeper.goToSleep();
}

void loop() {
  yield();
}

void doOnOrOFF(boolean on){
  bool desiredLedStatus = on;
  int retriesLeft = 3;
  onOffSensor.setup();
  auto actualStatusRule = []() {
            int read = onOffSensor.read();
            return read > VIN_THRESOLD;
        };
  while (desiredLedStatus != actualStatusRule() && retriesLeft > 0){
    retriesLeft -- ;
    pusher.push(CLICK_POSITION, RELEASE_POSITION, CLICK_TIME_MILES);
    if (desiredLedStatus == actualStatusRule()) break;
    delay (500);
  }
}

void commResponseCallback( response & rsp){
  if  (requests.operations_count == rsp.opResponses_count){

    int cmdResultCode = rsp.opResponses[0].result_code;
    if (cmdResultCode == response_Result_OK){
      String command (rsp.opResponses[0].payload);
      if(command.equalsIgnoreCase("click")){
        pusher.push(CLICK_POSITION, RELEASE_POSITION, CLICK_TIME_MILES);
        updateStatus();
      } else if (command.equalsIgnoreCase("on")){
        doOnOrOFF(true);
        updateStatus();
      } else if (command.equalsIgnoreCase("off")){
        doOnOrOFF(false);
        updateStatus();
      } 
    } 

    int sleepCode = rsp.opResponses[2].result_code;
    if (sleepCode == response_Result_OK){
      String readstr (rsp.opResponses[2].payload);
      int readn = readstr.toInt();
      if (readn >0 ){
        Sleep_sleepTimeMillis = readn;
      }
      updateStatus();
    }

    weHaveEspNowResponse = true; //unknown command
  } 
}

//TODO: send and check the sleepMillis queue
/*
void mqttCallback(char* topic, byte* payload, unsigned int length) {
  // put command on a string
  char command[length+1];
  mqtt_readPayloadToString(payload, command, length);
  
  //process options
  if(strcmp("ai/input/cmd",topic)==0 && strcmp("click",command)==0){
    pusher.push(CLICK_POSITION, RELEASE_POSITION, CLICK_TIME_MILES);
    mqttCom.cmdIdle(topic);
    updateStatus();
  }
  else if(strcmp("ai/input/cmd",topic)==0 && strcmp("on",command)==0){
    doOnOrOFF(true);
    mqttCom.cmdIdle(topic);
    updateStatus();
  }
  else if(strcmp("ai/input/cmd",topic)==0 && strcmp("off",command)==0){
    doOnOrOFF(false);
    mqttCom.cmdIdle(topic);
    updateStatus();
  }
  else if(strcmp("ai/input/cmd",topic)==0 && strcmp("ping",command)==0){
    mqttCom.cmdIdle(topic);
  }
  else if(strcmp("ai/input/sleepMillis",topic)==0 && strcmp("",command)!=0) 
  {
    String readstr = String(command);
    int readn = readstr.toInt();
    mqttCom.cmdIdle(topic);
    if (readn >0 ){
      Sleep_sleepTimeMillis = readn;
    }
  }
}
*/

